from selenium import webdriver
import time
from bs4 import BeautifulSoup

driver = webdriver.Chrome()
driver.get("https://pizza.de/lieferservice/mainz/55120/")  # Beispielort

last_height = driver.execute_script("return document.body.scrollHeight")
while True:
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(0.5)
    new_height = driver.execute_script("return document.body.scrollHeight")
    if new_height == last_height:
        break
    last_height = new_height

webpage = BeautifulSoup(driver.page_source.encode('utf-8'), 'html.parser')

for restaurant in webpage.find_all('li', class_="restaurant-list__item"):
    link = "https://pizza.de" + restaurant.find('a', class_="restaurant-item__cover-link link")['href']
    driver.get(link)
    time.sleep(0.5)
    driver.find_element_by_xpath("//div[@class='link' and @title='Impressum']").click()

    impressum_webpage = BeautifulSoup(driver.page_source.encode('utf-8'), 'html.parser')

    impressum_infos = impressum_webpage.find('div', class_="restaurant-impressum__section__row")

    infos = ' | '.join(info.find_all('td')[1].string for info in impressum_infos.find_all('tr'))

    print(infos)
driver.quit()
